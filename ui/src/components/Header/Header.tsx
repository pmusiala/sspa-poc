import styled from 'styled-components';

export const Header = styled.div`
    width: 100%;
    height: 50px;
    color: white;
    background: #251654;
    margin: auto;
    display: flex;
    align-items: center;
    font-family: 'Arial';
    padding: 13px;
    box-sizing: border-box;
`