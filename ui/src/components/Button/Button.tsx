import styled from 'styled-components';

export const Button = styled.div`
    background-color: #251654;
    border-radius: 4px;
    border: 2px solid white;
    font-weight: bold;
    text-align: center;
    padding: 5px;
    &:hover{
        background-color: grey;
        cursor: pointer; 
    }
`;