import {registerApplication, start} from "single-spa";
import {
    constructApplications,
    constructRoutes,
    constructLayoutEngine,
} from "single-spa-layout";
import microfrontendLayout from "./microfrontend-layout.html";

// this is needed to load global css
// import '@poc/ui';

import {Subject} from 'rxjs';
const store = new Subject();
const routes = constructRoutes(microfrontendLayout,
    {
        errors: {
            header: 'Couldn\'t load header component'
        },
        loaders: {
            header: 'loading...'
        },
        props: {
            store: store,
            secretKey: '23874293847293874923874823'
        }
    });

const applications = constructApplications({
    routes,
    loadApp({name}) {
        return System.import(name);
    },

});
const layoutEngine = constructLayoutEngine({routes, applications});

applications.forEach(registerApplication);

// Turn on layout listeners e.g. route change
layoutEngine.activate();

// Starts app, can be delayed
start();
