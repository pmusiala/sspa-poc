module.exports = {
    entry: {
        logger: './src/logger.js'
    },
    output: {
        libraryTarget: 'system',
        publicPath: ''
    },
    devServer: {
        headers: {
            "Access-Control-Allow-Origin": "*"
        }
    },
    mode: 'development'
}
