import { registerApplication, start } from "single-spa";

registerApplication({
  name: "@single-spa/welcome",
  app: () =>
    System.import(
      '@single-spa/welcome'
    ),
  activeWhen: ["/"],
});

registerApplication({
  name: '@poc/header',
  app: () => System.import('@poc/header'),
  activeWhen: ['/header']
})

// registerApplication({
//   name: "@poc/navbar",
//   app: () => System.import("@poc/navbar"),
//   activeWhen: ["/"]
// });

start({
  urlRerouteOnly: true,
});
