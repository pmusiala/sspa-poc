import { Header, Button } from '@poc/ui';
import { log } from '@poc/logger';

export default function Root(props) {
    return <Header>
        <Button onClick={() => {
            log('home clicked');
        }}>
            Home
        </Button>
    </Header>;
}
